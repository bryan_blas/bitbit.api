<?php
define('SITE_NAME',  'bitbit.cash');
define('SITE_TAGLINE',  'Take your cash wherever you go!');

define('INFO_EMAIL',  'info@bitbit.cash');
define('SUPPORT_EMAIL',  'support@bitbit.cash');
define('ADMIN_EMAIL',  'admin@bitbit.cash');

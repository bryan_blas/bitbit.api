<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Txn;
use DB;

class BankDeposit extends Model {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'bank_deposits';
	
	protected $fillable = ['user_id', 'bank_name', 'deposit_date', 'reference_code', 'amount', 'image', 'status_id', 'system_bank_id'];
	
	public static $active = 2;
	public static $completed = 6;

	public static function deposit($data) {
		$deposit = DB::table('bank_deposits')->insert(
			[
				'user_id' => $data['user_id'], 
				'deposit_date' => $data['deposit_date'], 
				'reference_code' => $data['reference_code'], 
				'amount' => $data['amount'],
				'image' => $data['image'],
				'status_id' => $data['status_id'],
				'system_bank_id' => $data['system_bank_id'],
				'created_at' => $data['created_at']
			]
		);
		return $deposit;
	} 

	public static function approve($deposit_id) {
		$bank_deposit = BankDeposit::find($deposit_id);
		
		//dd($bank_deposit);
		
		if ($bank_deposit) {
			Txn::creditBankDeposit($bank_deposit);
		
			$bank_deposit->status_id = self::$completed;
			$bank_deposit->save();
		}	
	} 

}

<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['firstname', 'lastname', 'email', 'password', 'email_code', 'verified_email', 'status_id', 'role_id', 'ip_address'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token', 'email_code', 'verified_email', 'status_id', 'role_id', 'ip_address'];
	
	public $timestamps = true;
	
	public static function getUserData($id)
	{
		$user = User::where('id', '=', $id)->first();
		
		if ($user) {
			return $user;
		}
		else {
			return null;
		}
	}

	public static function getUserByEmail($email)
	{
		$user = User::where('email', '=', $email)->first();
		
		if ($user) {
			return $user;
		}
		else {
			return null;
		}
	}

	
	public static function isBitbitUser($email)
	{
		$user = User::where('email', '=', $email)->first();
		
		if ($user) {
			return $user;
		}
		else {
			return null;
		}
		
	}

	public static function getRecipient($user_id)
	{
		$user = User::where('id', '=', $user_id)->first();
		
		if ($user->firstname && $user->lastname) {
			return $user->firstname . ' ' . $user->lastname;
		}
		else {
			return $user->email;
		}
		
	}

	
	public static function createPendingUser($email)
	{
		$user = User::create(array(
            'email' => $email,
            'status' => 4
        ));
	
	}
	

}

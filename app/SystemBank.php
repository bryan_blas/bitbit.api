<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class SystemBank extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'system_banks';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['account_name', 'account_number', 'status_id', 'bank_id'];
	
	public static $active = 2;

	public static function activeBanks()
	{
		$system_banks = DB::table('system_banks')
			->join('banks', 'banks.id', '=', 'system_banks.bank_id')
			->where('status_id', '=', self::$active)
			->get();
		
		return $system_banks;	
	}
	
}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class RequestMoney extends Model {

	//
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'requests';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['requester_id', 'recipient_id', 'amount', 'status_id', 'note'];
	
	public static function getRequest($request_id) {
		$request = DB::table('requests')
			->join('users', 'users.id', '=', 'requests.recipient_id')
			->join('statuses', 'statuses.id', '=', 'requests.status_id')
			->where('requests.id', '=', $request_id)
			->get();
			
		return $request;
	} 
	
	public static function payRequest($request_id) {
		$request = RequestMoney::find($request_id);
		$request->status_id = 5;
		$request->save();
	} 
	
}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Txn extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'txns';
	
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['txn_type_id', 'origin_id', 'recipient_id', 'amount', 'status_id', 'note'];
	
	public static $completed = 6;
	public static $send = 2;
	public static $bank_deposit = 1;
	public static $pending = 4;
	public static $initial_peso = 50;
	public static $bitbit_id = 2;

	public static function creditInitialPeso($user_id) {
		$credit_peso = DB::table('txns')->insert(
			[
				'txn_type_id' => self::$send, 
				'origin_id' => self::$bitbit_id, 
				'amount' => self::$initial_peso,
				'recipient_id' => $user_id,
				'status_id' => self::$completed, 
				'note' => 'Initial money',
				'created_at' => date("Y-m-d H:i:s")
			]
		);
		return $credit_peso;
	}
	
	public static function creditBankDeposit($deposit) {
		$credit_deposit = DB::table('txns')->insert(
			[
				'txn_type_id' => self::$bank_deposit, 
				'origin_id' => $deposit->id, 
				'amount' => $deposit->amount,
				'recipient_id' => $deposit->user_id,
				'status_id' => self::$completed, 
				'created_at' => date("Y-m-d H:i:s")
			]
		);
		return $credit_deposit;
	} 
	
	public static function getTransaction($txn_id) {
		$txn = DB::table('txns')
			->join('users', 'users.id', '=', 'txns.recipient_id')
			->join('statuses', 'statuses.id', '=', 'txns.status_id')
			->where('txns.id', '=', $txn_id)
			->get();
			
		return $txn;
	}
	
	public static function getTransactions($user_id) {
		$txn = DB::table('txns')
			->join('users as recipient', 'recipient.id', '=', 'txns.recipient_id')
			->join('users as origin', 'origin.id', '=', 'txns.origin_id')
			->join('statuses', 'statuses.id', '=', 'txns.status_id')
			->where('txns.origin_id', '=', $user_id)
			->orWhere('txns.recipient_id', '=', $user_id)
			->select('txns.id', 'txns.txn_type_id', 'txns.origin_id', 'txns.recipient_id', 'amount', 'note', 'txns.status_id', 'txns.created_at', 'recipient.firstname as recipient_fname', 'recipient.lastname as recipient_lname', 'recipient.pic as recipient_pic', 'recipient.email as recipient_email', 'origin.firstname as origin_fname', 'origin.lastname as origin_lname', 'origin.pic as origin_pic', 'origin.email as origin_email')
			->get();
			
		return $txn;
	} 
	 
	
	public static function pesoBalance($user_id) {
		$peso_balance = self::getUserCredit($user_id) - self::getUserDebit($user_id);
		return $peso_balance;
	}
	
	public static function getUserCredit($user_id) {
		$user_credit = DB::table('txns')->where('recipient_id', $user_id)->where('status_id', self::$completed)->sum('amount');
		return $user_credit;
	}

	public static function getUserDebit($user_id) {
		$user_debit = DB::table('txns')->where('origin_id', $user_id)->where('status_id', self::$completed)->sum('amount');
		return $user_debit;
	}
}

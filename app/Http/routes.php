<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'WelcomeController@index');
	
Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


// API ROUTES ==================================  
Route::group(array('prefix' => 'api'), function() {
	
	Route::get('/user/index', 'UserController@index');
    Route::post('/user/signup', 'UserController@signup');
	Route::post('/user/verify', 'UserController@verify');
	Route::post('/user/login', 'UserController@login');
	Route::post('/user/friends', 'UserController@friends');
	
	Route::get('/systemBank/activeBanks', 'SystemBankController@activeBanks');
	
	Route::post('/user/pesoBalance', 'UserController@pesoBalance');
	
	Route::post('/bankDeposit/deposit', 'BankDepositController@deposit');
	
	Route::post('/transaction/sendPeso', 'TxnController@sendPeso');
	
	Route::post('/transaction/get', 'TxnController@get');
	
	Route::post('/request/sendRequest', 'RequestMoneyController@sendRequest');
	
	Route::post('/request/pending', 'RequestMoneyController@pending');
	
	
});
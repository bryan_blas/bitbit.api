<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Txn;
use App\User;
use App\RequestMoney;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use Mail;

class TxnController extends Controller {
	
	public function sendPeso(Request $request) 
	{
		$send = 2;
		$completed = 6;
		
		$v = Validator::make($request->all(), [
        	'origin_id'		   => 'required',
        	'amount' 		   => 'required',
        	'email' 		   => 'required',	
    	]);

	    if ($v->fails()) {
	    	//$messages = $v->messages();
	        return response()->json(array('error' => $v->errors()),422);
   		} 
   		else {
   			
			$origin_id = $request->json('origin_id');
			$amount = $request->json('amount');
			$email = $request->json('email'); //email of recipient
			$note = $request->json('note');
			$request_id = $request->json('request_id');
			
			$recipient = User::getUserByEmail($email);
			//dd($origin_id);
			if ($recipient) {
				$txn = Txn::create(array(
		  		    'txn_type_id' => $send,
		            'origin_id' => $origin_id,
		            'amount' => $amount,
		            'recipient_id' => $recipient->id,
		            'status_id' => $completed,
		            'note' => $note
		        ));
				
				if ($txn) {
					if ($request_id) {
						RequestMoney::payRequest($request_id);
					}
					$data = Txn::getTransaction($txn->id);
					
					$sender = User::getUserData($origin_id);
					$recipient = User::getUserData($recipient->id);
					$recipient_name = User::getRecipient($recipient->id);
					
					//send peso email
					$mail_data = array(
						'firstname' => $sender->firstname,
						'lastname' => $sender->lastname,
						'email' => $sender->email,
						'amount' => $amount,
						'recipient' => $recipient_name,
						'subject' => 'You sent Php' . $amount . ' to ' . $recipient_name . '', 
						'confirm_link' => env('APP_URL') . '/view/transaction/'
					);
			
					Mail::send('emails.send_pesos', $mail_data, function($message) use ($mail_data)
					{
					    $message->to($mail_data['email'], $mail_data['firstname'] . ' ' . $mail_data['lastname'])
					    ->subject($mail_data['subject']);
					});
					
					//receive peso email
					$mail_data = array(
						'firstname' => $recipient->firstname,
						'lastname' => $recipient->lastname,
						'email' => $recipient->email,
						'amount' => $amount,
						'sender' => $sender->firstname . ' ' . $sender->lastname,
						'subject' => 'You received Php' . $amount . ' from ' . $sender->firstname . ' ' . $sender->lastname . '', 
						'confirm_link' => env('APP_URL') . '/view/transaction/'
					);
			
					Mail::send('emails.receive_pesos', $mail_data, function($message) use ($mail_data)
					{
					    $message->to($mail_data['email'], $mail_data['firstname'] . ' ' . $mail_data['lastname'])
					    ->subject($mail_data['subject']);
					});
					
			        return response()->json(array('success' => true, 'data' => $data));
				}
			}
			// if not bitbit user
			else {
					
			}
		}
	}

	public function get(Request $request) 
	{
		$user_id = $request->json('user_id');
		
		$data = Txn::getTransactions($user_id);
		
		if ($data) {
			return response()->json(array('success' => true, 'data' => $data));
		}  
		else {
			return response()->json(array('error' => 'No record found.'),405);
		}
	}
}

<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Txn;
use App\User;
use App\RequestMoney;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use Mail;
use DB;

class RequestMoneyController extends Controller {
	
	public function sendRequest(Request $request) 
	{
		$send = 2;
		$completed = 6;
		$pending = 4;
		
		$v = Validator::make($request->all(), [
        	'requester_id'		   => 'required',
        	'amount' 		   => 'required',
        	'email' 		   => 'required',	
    	]);

	    if ($v->fails()) {
	    	//$messages = $v->messages();
	        return response()->json(array('error' => $v->errors()),422);
   		} 
   		else {
   			
			$requester_id = $request->json('requester_id');
			$amount = $request->json('amount');
			$email = $request->json('email'); //email of recipient
			$note = $request->json('note');
			
			$recipient = User::getUserByEmail($email);
			//dd($note);
			if ($recipient) {
				$request = RequestMoney::create(array(
		            'requester_id' => $requester_id,
		            'amount' => $amount,
		            'recipient_id' => $recipient->id,
		            'note' => $note,
		            'status_id' => $pending
		        ));
				
				$data = RequestMoney::getRequest($request->id);
				
				$requester = User::getUserData($requester_id);
				$recipient = User::getUserData($recipient->id);
				$recipient_name = User::getRecipient($recipient->id);
				
				//send requester email
				$mail_data = array(
					'firstname' => $requester->firstname,
					'lastname' => $requester->lastname,
					'email' => $requester->email,
					'amount' => $amount,
					'recipient' => $recipient_name,
					'subject' => 'You requested Php' . $amount . ' to ' . $recipient_name . '', 
					'confirm_link' => env('APP_URL') . '/view/transaction/'
				);
		
				Mail::send('emails.requester_request', $mail_data, function($message) use ($mail_data)
				{
				    $message->to($mail_data['email'], $mail_data['firstname'] . ' ' . $mail_data['lastname'])
				    ->subject($mail_data['subject']);
				});
				
				//recipient request email
				$mail_data = array(
					'firstname' => $recipient->firstname,
					'lastname' => $recipient->lastname,
					'email' => $recipient->email,
					'amount' => $amount,
					'requester' => $requester->firstname . ' ' . $requester->lastname,
					'subject' => $requester->firstname . ' ' . $requester->lastname .' is requesting Php' . $amount . '', 
					'confirm_link' => env('APP_URL') . '/view/transaction/'
				);
		
				Mail::send('emails.recipient_request', $mail_data, function($message) use ($mail_data)
				{
				    $message->to($mail_data['email'], $mail_data['firstname'] . ' ' . $mail_data['lastname'])
				    ->subject($mail_data['subject']);
				});
				
		        return response()->json(array('success' => true, 'data' => $data));
			}
		}
		
	}
	public function pending(Request $request)
	{
		$pending = 4;
		
		$recipient_id = $request->json('user_id');
		
		//$pending_requests = DB::table('requests')->where('recipient_id', $recipient_id)->where('status_id', $pending);
		//dd($pending_requests);
		$pending_requests = DB::table('requests')
			->join('users', 'users.id', '=', 'requests.requester_id')
			->where('requests.recipient_id', $recipient_id)	
			->where('requests.status_id', $pending)
			->select('requests.id', 'requester_id', 'recipient_id', 'amount', 'note', 'requests.status_id', 'requests.created_at', 'firstname', 'lastname', 'email', 'pic')
			->get();
			
		if($pending_requests) {
			return response()->json(array('success' => true, 'data' => $pending_requests),200);	
		}
		else{
			return response()->json(array('error' => 'No record found'),405);
		}
	}
}

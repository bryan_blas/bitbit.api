<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Txn;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use Mail;
use Hash;
use DB;

class UserController extends Controller {
	
	public static $user_role = 3; //normal user 
	public static $inactive = 1; //normal user
	public static $active_user = 2; //normal user	
		
	public function index()
	{
        return response()->json(array('success' => true));
	}	

	public function signup(Request $request)
	{
		$v = Validator::make($request->all(), [
			'firstname' 	   => 'required',
        	'lastname'		   => 'required',
        	'email'            => 'required|email|unique:users',     // required and must be unique in the users table
        	'password'         => 'required',
        	'retype_password'  => 'required|same:password'
    	]);

	    if ($v->fails()) {
	    	//$messages = $v->messages();
	        return response()->json(array('error' => $v->errors()),422);
   		} 
   		else {
   			
			$firstname = $request->json('firstname');
			$lastname = $request->json('lastname');
			$email = $request->json('email');
			$password = Hash::make($request->json('password'));
			$email_code = md5(uniqid(rand(), true));
			$verified_email = 1;
			
			//echo $email_code;
			$user = User::create(array(
	  		    'firstname' => $firstname,
	            'lastname' => $lastname,
	            'email' => $email,
	            'password' => $password,
	            'email_code' => $email_code,
	            'verified_email' => $verified_email,
	            'status_id' => self::$inactive,
	            'role_id' => self::$user_role
	        ));
			
			//send activation email
			$mail_data = array(
				'firstname' => $firstname,
				'lastname' => $lastname,
				'email' => $email,
				'subject' => 'Welcome to bitbit! Please confirm your email address',
				'confirm_link' => env('APP_URL') . '/verify?code=' . $email_code
			);
	
			Mail::send('emails.welcome', $mail_data, function($message) use ($mail_data)
			{
			    $message->to($mail_data['email'], $mail_data['firstname'] . ' ' . $mail_data['lastname'])
			    ->subject($mail_data['subject']);
			});
			
	        return response()->json(array('success' => true));
		}
	}

	public function verify(Request $request)
	{
		$code = $request->json('code');
		
		$user = User::where('email_code', '=', $code)
		->where('verified_email', '=', 0)
		->first();
		
		if (!is_null($user)){
			$user->email_code = null;
			$user->verified_email = self::$active_user;
			$user->save();
			
			Txn::creditInitialPeso($user->id);
			
			return response()->json(array('success' => true));	
		}
		else {
			return response()->json(array('error' => 'Invalid verification code'),406);
		}
	}
	
	public function login(Request $request)
	{
		$email = $request->json('email');
		$password = $request->json('password');
		
		$user = User::where('email', '=', $email)->first();
		
		if ($user) {
			if ($user->verified_email) {
				if (Hash::check($password, $user->password)) {
					return response()->json(array('success' => true, 'data' => $user));
				}
				else {
					return response()->json(array('error' => 'Password not match'),401);
				}
			}
			else {
				return response()->json(array('error' => 'Email not verified. Please verify email first'),401);
			}
		}
		else {
			return response()->json(array('error' => 'User not found'),404);
		}
	}
	
	public function pesoBalance(Request $request)
	{
		$user_id = $request->json('user_id');
		
		$peso_balance = Txn::pesoBalance($user_id);
		
		return response()->json(array('success' => true, 'peso_balance' => $peso_balance ),200);
	}

	public function friends(Request $request) {
		$user_id = $request->json('user_id');
		
		//$friends = DB::
		$friends = DB::table('users')->where('id', "!=", $user_id)->where('status_id', self::$active_user)->where('role_id', self::$user_role)->get();
		
		return response()->json(array('success' => true, 'data' => $friends ),200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
}

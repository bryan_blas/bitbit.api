<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\BankDeposit;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BankDepositController extends Controller {
	
	public function deposit(Request $request)
	{
		$pending = 4;
		$completed = 6;
		
		$data = 
		[
			'user_id' => $request->json('user_id'),
			'deposit_date' => date("Y-m-d H:i:s"),
			'reference_code' => $request->json('reference_code'),
			'amount' => $request->json('amount'),
			'image' => 'test',
			'status_id' => $pending,
			'system_bank_id' => $request->json('system_bank_id'),
			'created_at' => date("Y-m-d H:i:s")
		];

		$deposit = BankDeposit::create($data);
		
		//dd($deposit->id);
		
		if ($deposit) {
			//auto approve deposit for demo purposes only
			$approve = BankDeposit::approve($deposit->id);
			return response()->json(array('success' => true), 200);
		}
		else {
			return response()->json(array('error' => 'Failed'),401);
		}	
			
	}
	
}	

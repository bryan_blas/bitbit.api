<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SystemBank;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SystemBankController extends Controller {
	
	public function activeBanks()
	{
		$system_banks = SystemBank::activeBanks();
		
		if ($system_banks) {
			return response()->json(array('success' => true, 'data' => $system_banks), 200);
		}
		else {
			return response()->json(array('error' => 'Bank not found'),400);
		}	
			
	}
	
}	

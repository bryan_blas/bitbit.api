<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Bitbit Welcome Email</title>
	<style>
	#outlook a{padding:0;}
	.ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
	.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
	body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}
	table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
	img{-ms-interpolation-mode:bicubic;display:block;}
	body{margin:0; padding:0;}
	img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
	table{border-collapse:collapse !important;}
	body{height:100% !important; margin:0; padding:0; width:100% !important;}
	a{ text-decoration: none; }
	img{ text-decoration: none; outline; }
	a img{ border:none; }
	body{ -webkit-font-smoothing: subpixel-antialiased;}
	td.contentblock p {
	  	color: #50595e;
		font-size: 16px;
		line-height: 26px;
		font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
		margin-top: 0;
		margin-bottom: 26px;
		padding-top: 0;
		padding-bottom: 0;
		font-weight: normal;
	}
	td.contentblock p a {
	  	color: #00dd9f;
		text-decoration: none;
	}
	td.contentfooter p {
		color: #66757F;
		font-size: 12px;
		line-height: 16px;
		font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
		margin-top: 0;
		margin-bottom: 0;
		padding-top: 0;
		padding-bottom: 0;
		font-weight: normal;
	}
	td.contentfooter p a {
		color: #66757F;
		text-decoration: none;
	}
	</style>
</head>
<body bgcolor="#ffffff" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" style="-webkit-font-smoothing: antialiased;width:100% !important;background:#ffffff;-webkit-text-size-adjust:none;">
	<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
		<tr>
			<td align="center" bgcolor="#ffffff" width="100%" height="30" nowrap><img style="display:block;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;-ms-interpolation-mode:bicubic;border-width:0;height:30px;line-height:100%;outline-style:none;text-decoration:none;border-style:none;" border="0" src="{{ url('img/email/spacer.png')}}" width="1" height="30" class="divider"></td>
		</tr>
		<tr>
			<td align="left" bgcolor="#ffffff" width="100%">
				<table width="620" cellpadding="0" cellspacing="0" border="0" align="center" class="table">
					<tr>
						<td width="520" height="80" nowrap align="center" valign="top" colspan="2">
							<a href="https://bitbit.cash" style="display:block;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;-ms-interpolation-mode:bicubic;border-width:0;height:46px;line-height:100%;outline-style:none;text-decoration:none;border-style:none;"><img style="display:block;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;-ms-interpolation-mode:bicubic;border-width:0;width:180px;height:46px;line-height:100%;outline-style:none;text-decoration:none;border-style:none;" border="0" src={{ url('img/email/bitbit-logo-email.png')}} width="180" height="46"></a>
						</td>
					</tr>
					<tr>
						<td colspan="2" width="100%">
							<table width="620" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td width="100%" bgcolor="#fff">
										<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td bgcolor="#fff" class="contentblock" align="left">
													<br/><br/>
													<p style="color:#50595e;font-size:14px;line-height:24px;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;margin-top:0;margin-bottom:25px;padding-top:0;padding-bottom:0;font-weight:normal;">
Hi {{ $firstname . ' ' . $lastname }},<br /><br />

Thanks for signing up for our Bitbit updates! Bitbit’s beta launch is still on the way. In the meantime, please confirm your account by clicking the button below.<br /><br /> 

<a href={{ $confirm_link }}>confirm email address</a><br /><br /> 

Alternately, you can also confirm your account by copying and pasting this URL onto your browser:<br /><br /> 

{{$confirm_link}}<br /><br />

Regards,<br />
{{ SITE_NAME}}<br />
{{ INFO_EMAIL }}
													</p>
												</td>
											</tr>
										</table>
									</td>
								</tr>

							</table>
						</td>
					</tr>
					<tr>
						<td colspan="2" width="100%" height="100" nowrap><img style="display:block;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;-ms-interpolation-mode:bicubic;border-width:0;height:100px;line-height:100%;outline-style:none;text-decoration:none;border-style:none;" border="0" src="{{ url('img/email/spacer.png')}}" width="90%" height="100"></td>
					</tr>
					<tr>
						<td colspan="2" width="100%" class="contentfooter" align="center">
							<br/><br/>
							<p style="color:#66757F;font-size:12px;line-height:16px;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0;font-weight:normal;">© 2015 <a style="color:#66757F;text-decoration:none;" href="http://bitbit.cash/">Bitbit.</a> All Rights Reserved.</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" bgcolor="#ffffff" width="100%" height="30" nowrap><img style="display:block;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;-ms-interpolation-mode:bicubic;border-width:0;height:30px;line-height:100%;outline-style:none;text-decoration:none;border-style:none;" border="0" src="{{ url('img/email/spacer.png')}}" width="90%" height="30"></td>
		</tr>
	</table>
</body>
</html>
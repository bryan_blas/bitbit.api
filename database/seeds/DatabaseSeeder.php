<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Role;
use App\Status;
use App\Bank;
use App\SystemBank;
use App\TxnType;
use \Hash;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		$this->call('UserTableSeeder');
		$this->call('RoleTableSeeder');
		$this->call('StatusTableSeeder');
		$this->call('BankTableSeeder');
		$this->call('SystemBankTableSeeder');
		$this->call('TxnTypeTableSeeder');
	}
}

class UserTableSeeder extends Seeder {

    public function run()
    {
    	DB::table('users')->delete();
        $users = [ 
        	[ 
	            'firstname' => 'Bryan',
	            'lastname' => 'Blas',
	            'email' => 'bryan@sci.ventures',
	            'password' => Hash::make('Test12345'),
	            'pic' => 'default.jpg',
	            'email_code' => '',
	            'verified_email' => 1,
	            'status_id' => 2,
	            'role_id' => 1,
	            'created_at' => date("Y-m-d H:i:s")
        	],
        	[ 
	            'firstname' => 'Bitbit',
	            'lastname' => 'Nakamoto',
	            'email' => 'bitbit@bitbit.cash',
	            'password' => Hash::make('Test12345'),
	            'pic' => 'bitoy.jpg',
	            'email_code' => '',
	            'verified_email' => 1,
	            'status_id' => 2,
	            'role_id' => 3,
	            'created_at' => date("Y-m-d H:i:s")
        	],
        	[ 
	            'firstname' => 'Bryan',
	            'lastname' => 'Blas',
	            'email' => 'bryan@sci.venture',
	            'password' => Hash::make('Test12345'),
	            'pic' => 'bryan.jpg',
	            'email_code' => '',
	            'verified_email' => 1,
	            'status_id' => 2,
	            'role_id' => 3,
	            'created_at' => date("Y-m-d H:i:s")
        	],    
        	[ 
	            'firstname' => 'Jomel',
	            'lastname' => 'Aramina',
	            'email' => 'jomel@sci.venture',
	            'password' => Hash::make('Test12345'),
	            'pic' => 'default.jpg',
	            'email_code' => '',
	            'verified_email' => 1,
	            'status_id' => 2,
	            'role_id' => 3,
	            'created_at' => date("Y-m-d H:i:s")
        	],
        	[ 
	            'firstname' => 'John',
	            'lastname' => 'Bailon',
	            'email' => 'john@sci.venture',
	            'password' => Hash::make('Test12345'),
	            'pic' => 'john.jpg',
	            'email_code' => '',
	            'verified_email' => 1,
	            'status_id' => 2,
	            'role_id' => 3,
	            'created_at' => date("Y-m-d H:i:s")
        	],    
        	[ 
	            'firstname' => 'Jardine',
	            'lastname' => 'Gerodias',
	            'email' => 'jardine@sci.venture',
	            'password' => Hash::make('Test12345'),
	            'pic' => 'jardine.jpg',
	            'email_code' => '',
	            'verified_email' => 1,
	            'status_id' => 2,
	            'role_id' => 3,
	            'created_at' => date("Y-m-d H:i:s")
        	],    
        	[ 
	            'firstname' => 'Sam',
	            'lastname' => 'Kaddoura',
	            'email' => 'sam@sci.venture',
	            'password' => Hash::make('Test12345'),
	            'pic' => 'sam.jpg',
	            'email_code' => '',
	            'verified_email' => 1,
	            'status_id' => 2,
	            'role_id' => 3,
	            'created_at' => date("Y-m-d H:i:s")
        	],    
          	[ 
	            'firstname' => 'Camill',
	            'lastname' => 'Vazquez',
	            'email' => 'camill@sci.venture',
	            'password' => Hash::make('Test12345'),
	            'pic' => 'camill.jpg',
	            'email_code' => '',
	            'verified_email' => 1,
	            'status_id' => 2,
	            'role_id' => 3,
	            'created_at' => date("Y-m-d H:i:s")
        	],    
        	[ 
	            'firstname' => 'Sabina',
	            'lastname' => 'Vergara',
	            'email' => 'sabina@sci.venture',
	            'password' => Hash::make('Test12345'),
	            'pic' => 'sabina.jpg',
	            'email_code' => '',
	            'verified_email' => 1,
	            'status_id' => 2,
	            'role_id' => 3,
	            'created_at' => date("Y-m-d H:i:s")
        	]    
        ];
        User::insert($users);
    }
}

class RoleTableSeeder extends Seeder {

    public function run()
    {
    	DB::table('roles')->delete();
        $roles = [ 
            ['name' => 'Super Admin', 'created_at' => date("Y-m-d H:i:s")],
            ['name' => 'Admin', 'created_at' => date("Y-m-d H:i:s")],
            ['name' => 'User', 'created_at' => date("Y-m-d H:i:s")]
        ];    
        
        Role::insert($roles);
    }
}


class StatusTableSeeder extends Seeder {

    public function run()
    {
    	DB::table('statuses')->delete();
        $statuses = [ 
            ['name' => 'Inactive', 'created_at' => date("Y-m-d H:i:s")],
            ['name' => 'Active', 'created_at' => date("Y-m-d H:i:s")],
            ['name' => 'Deactivated', 'created_at' => date("Y-m-d H:i:s")],
            ['name' => 'Pending', 'created_at' => date("Y-m-d H:i:s")],
            ['name' => 'Paid', 'created_at' => date("Y-m-d H:i:s")],
            ['name' => 'Completed', 'created_at' => date("Y-m-d H:i:s")],
            ['name' => 'Expired', 'created_at' => date("Y-m-d H:i:s")],
            ['name' => 'For Remittance', 'created_at' => date("Y-m-d H:i:s")],
            ['name' => 'Remitted', 'created_at' => date("Y-m-d H:i:s")],
        ];    
        
        Status::insert($statuses);
    }
}

class BankTableSeeder extends Seeder {

    public function run()
    {
    	DB::table('banks')->delete();
        $banks = [
        	['bank_name' => 'Banco de Oro Unibank', 'short_name' => 'BDO', 'created_at' => date("Y-m-d H:i:s")], 
            ['bank_name' => 'Bank of the Philippine Islands', 'short_name' => 'BPI', 'created_at' => date("Y-m-d H:i:s")],
            ['bank_name' => 'Metropolitan Bank and Trust Company', 'short_name' => 'Metrobank', 'created_at' => date("Y-m-d H:i:s")],
            ['bank_name' => 'China Bank Corporation', 'short_name' => 'China Bank', 'created_at' => date("Y-m-d H:i:s")],
            ['bank_name' => 'Land Bank of the Philippines', 'short_name' => 'Land Bank', 'created_at' => date("Y-m-d H:i:s")],
            ['bank_name' => 'Union Bank of the Philippines', 'short_name' => 'Union Bank', 'created_at' => date("Y-m-d H:i:s")],
            ['bank_name' => 'East West Banking Corporation', 'short_name' => 'East West', 'created_at' => date("Y-m-d H:i:s")]
        ];    
        
        Bank::insert($banks);
    }
}

class SystemBankTableSeeder extends Seeder {

    public function run()
    {
    	DB::table('system_banks')->delete();
        $system_banks = ['account_name' => 'Satoshi Citadel Industries', 'account_number' => '055-7-05554980-6', 'status_id' => '2', 'bank_id' => '1', 'created_at' => date("Y-m-d H:i:s")];    
        
        SystemBank::insert($system_banks);
    }
}

class TxnTypeTableSeeder extends Seeder {

    public function run()
    {
    	DB::table('txn_types')->delete();
        $txn_types = [ 
        	['name' => 'Bank Deposit', 'description' => '', 'created_at' => date("Y-m-d H:i:s")],
        	['name' => 'Send', 'description' => '', 'created_at' => date("Y-m-d H:i:s")],
        	['name' => 'Request', 'description' => '', 'created_at' => date("Y-m-d H:i:s")]
        ];
        
        TxnType::insert($txn_types);
    }
}

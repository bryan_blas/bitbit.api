<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemBanksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('system_banks', function(Blueprint $table)
		{
            $table->increments('id');
			$table->string('account_name', 255);
			$table->string('account_number', 255);
			$table->tinyInteger('status_id');
            $table->tinyInteger('bank_id');
            $table->timestamps();
            $table->softDeletes();
		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}

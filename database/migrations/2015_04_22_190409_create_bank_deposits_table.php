<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankDepositsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bank_deposits', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->timestamp('deposit_date');
			$table->string('reference_code');
			$table->double('amount');
			$table->string('image');
			$table->tinyInteger('status_id');
			$table->tinyInteger('system_bank_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bank_deposits');
	}

}

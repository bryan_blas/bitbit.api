<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTxnsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('txns', function(Blueprint $table)
		{
            $table->increments('id');
			$table->tinyInteger('txn_type_id');
			$table->integer('origin_id');
			$table->integer('recipient_id');
			$table->double('amount');
			$table->string('note')->nullable();
			$table->tinyInteger('status_id');
            $table->timestamps();
            $table->softDeletes();
		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
